"""Django REST Framework views module (implementing API endpoints)"""
# pylint: disable=too-many-ancestors, no-member, unused-argument

import random as pyrandom

import django_filters.rest_framework
from rest_framework import viewsets, filters
from rest_framework.decorators import action
from rest_framework.response import Response

from .models import Restaurant
from .serializers import RestaurantSerializer

class RestaurantViewSet(viewsets.ModelViewSet):
    # No docstring -- it would appear in the '/docs' under
    # each individual functionality
    _filter_fields = ['restaurant']
    queryset = Restaurant.objects.all().order_by('restaurant')
    serializer_class = RestaurantSerializer
    filter_backends = (django_filters.rest_framework.DjangoFilterBackend, filters.SearchFilter,)
    filterset_fields = _filter_fields
    search_fields = _filter_fields

    @action(methods=['get'], detail=False, url_path='random')
    def random_resto(self, request):
        """Return a random choice restaurant"""
        all_restos = self.get_queryset()
        if not all_restos:
            return Response()
        resto = pyrandom.choice(all_restos)
        return Response(self.get_serializer(resto).data)
