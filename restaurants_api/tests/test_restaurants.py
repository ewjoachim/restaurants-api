"""Unittest module for the Restaurant API"""

import pytest

from rest_framework.test import APIClient

CLIENT = APIClient()


@pytest.mark.django_db
def test_list():
    """Testing the list function"""
    resp = CLIENT.get('/restaurants/', format='json')
    assert resp.status_code == 200
    restos = resp.json()
    assert restos
    assert 'restaurant' in restos[0].keys()

@pytest.mark.django_db
def test_get_by_id():
    """Getting object by ID"""
    resp = CLIENT.get('/restaurants/1/', format='json')
    assert resp.status_code == 200
    resto = resp.json()
    assert resto
    assert 'restaurant' in resto.keys()
    assert resto['restaurant'] == 'testresto1'

@pytest.mark.django_db
def test_get_nonexistent():
    """Aiming to retrieve a non-existing object"""
    resp = CLIENT.get('/restaurants/3/', format='json')
    assert resp.status_code == 404

@pytest.mark.django_db
def test_get_random():
    """Testing the random restaurant functionality"""
    resp = CLIENT.get('/restaurants/random/', format='json')
    assert resp.status_code == 200
    assert 'restaurant' in resp.json().keys()


@pytest.mark.django_db
def test_post_delete():
    """Testing the POST and the DELETE function"""

    # OK, let's add the new item
    resp = CLIENT.post('/restaurants/', {'restaurant': 'To the Hungry Wonderer'}, format='json')
    assert resp.status_code == 201

    new_resto = resp.json()
    resto_id = new_resto['id']

    # Really there?
    resp = CLIENT.get('/restaurants/{}/'.format(resto_id), format='json')
    assert resp.status_code == 200

    # Fine, let's delete it
    resp = CLIENT.delete('/restaurants/{}/'.format(resto_id), format='json')
    assert resp.status_code in [202, 204]

    # Surely not there
    resp = CLIENT.get('/restaurants/{}/'.format(resto_id), format='json')
    assert resp.status_code == 404


@pytest.mark.django_db
def test_filter():
    """Testing the filter function for restaurant lists"""

    # OK, let's add the new item
    resp = CLIENT.get('/restaurants/', {'restaurant': 'testresto1'}, format='json')
    assert resp.status_code == 200
    resto = resp.json()
    assert resto
    assert 'id' in resto[0].keys()
    assert resto[0]['id'] == 1


@pytest.mark.django_db
def test_search():
    """Testing the search function for restaurant lists"""

    # OK, let's add the new item
    resp = CLIENT.get('/restaurants/', {'search': 'resto'}, format='json')
    assert resp.status_code == 200
    resto = resp.json()
    assert len(resto) == 2
