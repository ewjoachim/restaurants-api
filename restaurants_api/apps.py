"""Django app config for the Restaurants app"""

from django.apps import AppConfig

class RestaurantConfig(AppConfig):
    name = 'restaurants_api'
